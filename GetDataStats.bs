#!/bin/bash

ResultsTXT="Stats.txt";

DatasetDIR="./Modified-Dataset";
MaleDataset="$DatasetDIR/Male";
FemaleDataset="$DatasetDIR/Female";

NumMales=$(ls -1q $MaleDataset | wc -l);
NumFemales=$(ls -1q $FemaleDataset | wc -l);
TotalAuthors=$(($NumMales + $NumFemales));

Thresh="90%";


echo "------------------------------" >> $ResultsTXT;

echo "Number Count Statistics" >> $ResultsTXT;
echo " " >> $ResultsTXT;
echo "{\"Total-Authors\":\"$TotalAuthors\",\"Num-Female-Authors\":\"$NumFemales\",\"Num-Male-Authors\":\"$NumMales\"}" >> $ResultsTXT;
echo " " >> $ResultsTXT;
UnfilteredDatasetDIR="./Unfiltered-Dataset";
UnfilteredMaleDataset="$UnfilteredDatasetDIR/Male-Dataset-Unfiltered";
UnfilteredFemaleDataset="$UnfilteredDatasetDIR/Female-Dataset-Unfiltered";



NumFilteredFemaleJavaFiles=$(find $FemaleDataset -type f -name "*.java" | wc -l);
NumFilteredMaleJavaFiles=$(find $MaleDataset -type f -name "*.java" | wc -l);
TotalFilteredJavaFiles=$(($NumFilteredFemaleJavaFiles + $NumFilteredMaleJavaFiles));

NumUnfilteredFemaleJavaFiles=$(find $UnfilteredFemaleDataset -type f -name "*.java" | wc -l);
NumUnfilteredMaleJavaFiles=$(find $UnfilteredMaleDataset -type f -name "*.java" | wc -l);
TotalUnfilteredJavaFiles=$(($NumUnfilteredFemaleJavaFiles + $NumUnfilteredMaleJavaFiles));

Size=$(du -hsb $DatasetDIR | cut -f1);
FemaleSize=$(du -hsb $FemaleDataset | cut -f1);
MaleSize=$(du -hsb $MaleDataset | cut -f1);

echo "------------------------------" >> $ResultsTXT;

echo "Filtered File Statistics" >> $ResultsTXT;
echo " " >> $ResultsTXT;
echo "{\"Total-Filtered-Files\":\"$TotalFilteredJavaFiles\",\"Total-Filtered-Male-Files\":\"$NumFilteredMaleJavaFiles\",\"Total-Filtered-Female-Files\":\"$NumFilteredFemaleJavaFiles\",\"Size-All-Filtered\":\"$Size\",\"Size-Female\":\"$FemaleSize\",\"Size-Male\":\"$MaleSize\"}" >> $ResultsTXT;
echo " " >> $ResultsTXT;
Size=$(du -hsb $UnfilteredDatasetDIR | cut -f1);
FemaleSize=$(du -hsb $UnfilteredFemaleDataset | cut -f1);
MaleSize=$(du -hsb $UnfilteredMaleDataset | cut -f1);

echo "------------------------------" >> $ResultsTXT;

echo "Unfiltered File Statistics" >> $ResultsTXT;
echo " " >> $ResultsTXT;
echo "{\"Total-Unfiltered-Files\":\"$TotalUnfilteredJavaFiles\",\"Total-Unfiltered-Male-Files\":\"$NumUnfilteredMaleJavaFiles\",\"Total-Unfiltered-Female-Files\":\"$NumUnfilteredFemaleJavaFiles\",\"Size-All-Unfiltered\":\"$Size\",\"Size-Female\":\"$FemaleSize\",\"Size-Male\":\"$MaleSize\"}" >> $ResultsTXT;
echo " " >> $ResultsTXT;
TotalFileLoss=$(($TotalUnfilteredJavaFiles - $TotalFilteredJavaFiles));
TotalFemaleFileLoss=$(($NumUnfilteredFemaleJavaFiles - $NumFilteredFemaleJavaFiles));
TotalMaleFileLoss=$(($NumUnfilteredMaleJavaFiles - $NumFilteredMaleJavaFiles));

echo "------------------------------" >> $ResultsTXT;

echo "File Loss Statistics" >> $ResultsTXT;
echo " " >> $ResultsTXT;
echo "{\"Threshold\":\"$Thresh\",\"Total-File-Loss\":\"$TotalFileLoss\",\"Total-Male-File-Loss\":\"$TotalMaleFileLoss\",\"Total-Female-File-Loss\":\"$TotalFemaleFileLoss\"}" >> $ResultsTXT;
echo " " >> $ResultsTXT;
echo "------------------------------" >> $ResultsTXT;
echo "Male Author Statistics" >> $ResultsTXT;
echo " " >> $ResultsTXT;

gender="Male";

for D in $MaleDataset/*; do
    echo "D == $D";
    NumFiles=$(find $D -type f -name "*.java" | wc -l);
    Author=$(basename "${D}");
    Size=$(du -hsb $D | cut -f1);
    echo "{\"Gender\":\"$gender\",\"Author\":\"$Author\",\"Num-Files\":\"$NumFiles\",\"Size\":\"$Size\"}" >> $ResultsTXT;
done

echo " " >> $ResultsTXT;

echo "------------------------------" >> $ResultsTXT;

echo "Female Author Statistics" >> $ResultsTXT;
echo " " >> $ResultsTXT;

gender="Female";

for D in $FemaleDataset/*; do
    NumFiles=$(find $D -type f -name "*.java" | wc -l);
    Author=$(basename "${D}");
    Size=$(du -hsb $D | cut -f1);
    echo "{\"Gender\":\"$gender\",\"Author\":\"$Author\",\"Num-Files\":\"$NumFiles\",\"Size\":\"$Size\"}" >> $ResultsTXT;
done

echo " " >> $ResultsTXT;

echo "------------------------------" >> $ResultsTXT;

echo "Male File Statistics" >> $ResultsTXT;
echo " " >> $ResultsTXT;

gender="Male";

for D in $MaleDataset/*; do
    for F in $D/*.java; do
        echo "FILE == $F";
        Author=$(basename "${D}");
        Filename=$(basename "${F}");
        Size=$(du -hsb $F | cut -f1);
        LOC=$(wc -l < $F);
        echo "{\"Author\":\"$Author\",\"Gender\":\"$gender\",\"File-Name\":\"$Filename\",\"Size\":\"$Size\",\"Lines-Of-Code\":\"$LOC\"}" >> $ResultsTXT;
    done
done


echo "------------------------------" >> $ResultsTXT;

echo "Female File Statistics" >> $ResultsTXT;
echo " " >> $ResultsTXT;

gender="Female";

for D in $FemaleDataset/*; do
    for F in $D/*.java; do
        echo "FILE == $F";
        Author=$(basename "${D}");
        Filename=$(basename "${F}");
        Size=$(du -hsb $F | cut -f1);
        LOC=$(wc -l < $F);
        echo "{\"Author\":\"$Author\",\"Gender\":\"$gender\",\"File-Name\":\"$Filename\",\"Size\":\"$Size\",\"Lines-Of-Code\":\"$LOC\"}" >> $ResultsTXT;
    done
done