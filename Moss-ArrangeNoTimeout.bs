#!/bin/bash

Gender="Female";

declare -i Counter=0;
declare -i NumCreationFolders=20;
declare -i NameCTR=1;

for DIR in $Gender-Dataset-Filtered/*; do
    Counter=0;
    NameCTR=1;
    NewDIR=$DIR-$NameCTR;
    mkdir -p $NewDIR;
    for file in $DIR/*; do
        if [ $Counter -lt $NumCreationFolders ]; then
            mv $file $NewDIR;
            Counter=$((Counter+1));
        else
            Counter=0;
            NameCTR=$((NameCTR+1));
            NewDIR=$DIR-$NameCTR;
            mkdir -p $NewDIR;
            mv $file $NewDIR;
        fi
    done
    rm -rf $DIR;
done
