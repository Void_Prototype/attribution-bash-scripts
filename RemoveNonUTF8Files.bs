#!/bin/bash

# SCRIPT ASSUMES that locale is set to UTF-8
# To check, do command "locale"

Directory="./40-40-dataset";
FileLangExt="java";

find $Directory/ -type f -name "*.$FileLangExt" | while IFS= read file_name; do
    if grep -axv '.*' $file_name; then
        echo " $file_name";
        rm -f $file_name;
        echo " ";
        echo " ":
    fi
done