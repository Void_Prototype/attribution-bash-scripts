#!/bin/bash

#Remove every file that contains ASCII characters not in the given rangea
LowerHEXRange="0"; #VALID ASCII START RANGE
UpperHEXRange="7F"; #VALID ASCII END RANGE
FileLangExt="txt";
Directory="./40-40-dataset";

find $Directory/ -type f -name "*.$FileLangExt" | while IFS= read file_name; do
  grep -qP "[^\x$LowerHEXRange-\x$UpperHEXRange]" $file_name && rm -f $file_name || echo "GOOD -- $file_name";
done
