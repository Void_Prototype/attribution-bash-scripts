#!/bin/bash

#--------------------
#Modifiable Vars
TargetFile="./countcommas.txt";
Delimeter=".";
#--------------------
#Unmodifiable Vars

#--------------------

NumDelimeters=$(fgrep -o $Delimeter $TargetFile | wc -l);
echo "Num '$Delimeter' Characters == $NumDelimeters";
