#!/bin/bash

FILEDIR="./Repo-Female-Dataset"
OUTPUT="1-Female-ID-From-Repo.txt"

for d in $FILEDIR/*; 
do
  [[ -d "$d" ]] && echo "${d##*/}," >> $OUTPUT;
done