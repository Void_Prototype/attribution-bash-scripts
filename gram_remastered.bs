#!/bin/bash

#------------------------------#
# AUTHORS:
# Mathew Cathcart
#------------------------------#


#------------------------------#
# ===== USAGE =====
# 
# Modifiable Variables:
#
#   -PathToDataset
#       -Path to the ROOT DIRECTORY of the
#        dataset you wish to run this script on
#       -Example: PathToDataset = "./Dataset"
#                 PathToDataset = "/usr/person/directory/Dataset"
#                 PathToDataset = "../../All-Datasets/Dataset"
#
#   -FileLangExt
#       -File extension you wish to read.
#       -Example: FileLangExt = "java"
#                 FileLangExt = "txt"
#       
#   -NumNGrams
#       -The number of N-Grams. Positive integers only
#------------------------------#


#------------------------------#
# ===== MODIFIABLE VARIABLES =====
#
PathToDataset="./50-50-Dataset";
FileLangExt="java";
NumNGrams="10";
#
#------------------------------#


#------------------------------#
# ===== NON-MODIFIABLE VARIABLES =====
#
NumbersFile="values.txt"; #Text File to store gram patterns
Gram="Gram";#Name for anything Gram Related
GramTargetsFile="targets.txt";
#
#------------------------------#


#------------------------------#
# ===== MAIN SCRIPT =====

echo "-----SCRIPT RUNNING-----";

mkdir -p $NumNGrams$Gram-Metadata;

echo "Finding all $FileLangExt files and making $NumNGrams$Gram txt files";
find $PathToDataset -type f -name "*.$FileLangExt" | while IFS= read file_name; do
    basename="${file_name%.*}";
    cat $file_name | tr -d '\n' | sed -e "s/.\{$NumNGrams\}/&\n/g" | sort > $basename-$NumNGrams$Gram.txt;
done

echo "Making targets file";
find $PathToDataset -type f -name "*$NumNGrams$Gram.txt" > $NumNGrams$Gram-Metadata/$NumNGrams$Gram-$GramTargetsFile;

echo "Making Numbers File";
xargs cat < $NumNGrams$Gram-Metadata/$NumNGrams$Gram-$GramTargetsFile | sort -u > $NumNGrams$Gram-Metadata/$NumNGrams$Gram-$NumbersFile;



#below code may break python script
#echo "Removing Emptylines from values and targets file";
#printf "%s" "$(<./$NumNGrams$Gram-Metadata/$NumNGrams$Gram-$NumbersFile)" > $NumNGrams$Gram-Metadata/$NumNGrams$Gram-$NumbersFile;
#printf "%s" "$(<./$NumNGrams$Gram-Metadata/$NumNGrams$Gram-$GramTargetsFile)" > $NumNGrams$Gram-Metadata/$NumNGrams$Gram-$GramTargetsFile;

#
#echo "Counting number of $NumNGrams grams in each target file and creating frequency file";
#while IFS= read file_name; do
#    echo "Working on File == $file_name";
#    cat $NumNGrams$Gram-$NumbersFile | xargs -d '\n' -I {} grep -Fc -- "{}" "$file_name" | xargs printf "%d," >> $NumNGrams$Gram-$FrequencyFile;
#    label=$(echo "$file_name" | cut -d '/' -f $LabelIndexDepth);
#    printf "$label\n" >> $NumNGrams$Gram-$FrequencyFile;
#    echo "Added Label == $label";
#done < $NumNGrams$Gram-$GramTargetsFile
#

echo "-----SCRIPT FINISHED-----";
#------------------------------#
