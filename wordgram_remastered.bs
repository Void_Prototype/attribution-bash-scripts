#!/bin/bash

#------------------------------#
# AUTHORS:
# Mathew Cathcart
#------------------------------#


#------------------------------#
# ===== USAGE =====
# 
# Modifiable Variables:
#
#   -PathToDataset
#       -Path to the ROOT DIRECTORY of the
#        dataset you wish to run this script on
#       -Example: PathToDataset = "./Dataset"
#                 PathToDataset = "/usr/person/directory/Dataset"
#                 PathToDataset = "../../All-Datasets/Dataset"
#
#   -FileLangExt
#       -File extension you wish to read.
#       -Example: FileLangExt = "java"
#                 FileLangExt = "txt"
#       
#   -NumWordGrams
#       -The number of Word-Grams. Positive integers only
#
#------------------------------#
# ===== MODIFIABLE VARIABLES =====
#
PathToDataset="./50-50-Dataset";
FileLangExt="java";
NumWordGrams="10";
#
#------------------------------#


#------------------------------#
# ===== NON-MODIFIABLE VARIABLES =====
#
NumbersFile="values.txt"; #Text File to store gram patterns
WordGram="Wordgram";#Name for anything Gram Related
GramTargetsFile="targets.txt";
counter="1";
#
#------------------------------#


#------------------------------#
# ===== MAIN SCRIPT =====

echo "-----SCRIPT RUNNING-----";

mkdir -p $NumWordGrams$WordGram-Metadata;

echo "Finding all $FileLangExt files and making $NumWordGrams$WordGram txt files";

( set -f
    find $PathToDataset -type f -name "*.$FileLangExt" | while IFS= read file_name; do
        basename="${file_name%.*}";
        #echo "File Name == $file_name";
        while read line; do
            for word in $line; do
                if [ "$counter" -lt "$NumWordGrams" ]; then
                    printf "%s" "$word" >> $basename-$NumWordGrams$WordGram.txt;
                else
                    printf "%s\n" "$word" >> $basename-$NumWordGrams$WordGram.txt;
                fi
            done
        done < $file_name
    done
)

echo "Making targets file $GramTargetsFile";
find $PathToDataset -type f -name "*$NumWordGrams$WordGram.txt" > $NumWordGrams$WordGram-Metadata/$NumWordGrams$WordGram-$GramTargetsFile;

echo "Making values file $NumbersFile";
xargs cat < $NumWordGrams$WordGram-Metadata/$NumWordGrams$WordGram-$GramTargetsFile | sort -u > $NumWordGrams$WordGram-Metadata/$NumWordGrams$WordGram-$NumbersFile;

echo "-----SCRIPT FINISHED-----";
#------------------------------#
