#!/bin/bash

#PROXY: curl -x http://us-wa.proxymesh.com:31280 -U Mathew.Cathcart:PASSWORD https://api.genderize.io/?name=$FIRSTNAME >> genderize-metadata/$ID/metadata-genderize.txt;
mkdir -p genderize-metadata

while IFS=, read ID;
do
  NAMEGREPRESULT="$(grep -Po '"name": *\K"[^"]*"' ../Git-Metadata/git-metadata/$ID/metadata-git.txt)";
  mkdir -p genderize-metadata/$ID;
  echo "USERNAME: $ID" >> genderize-metadata/$ID/metadata-genderize.txt;
  echo "" >> genderize-metadata/$ID/metadata-genderize.txt;
  NAME="${NAMEGREPRESULT%\"}";
  NAME="${NAME#\"}";
  echo "" >> genderize-metadata/$ID/metadata-genderize.txt;
  echo "FULL NAME: $NAME" >> genderize-metadata/$ID/metadata-genderize.txt;
  FIRSTNAME="$(echo $NAME | head -n1 | awk '{print $1;}')";
  curl https://api.genderize.io/?name=$FIRSTNAME >> genderize-metadata/$ID/metadata-genderize.txt;
done < ../id-all.txt
