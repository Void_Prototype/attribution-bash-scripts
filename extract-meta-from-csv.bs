#!/bin/bash

mkdir -p DB-metadata

while IFS=, read -r col1 col2 col3 col4 col5;
do
  if [ "$col3" = "Java" ]; then
	mkdir -p DB-metadata/$col1;
	echo "{\"repo_name\":\"$col1\",\"owner_name\":\"$col2\",\"language\":\"$col3\"}" > ./DB-metadata/$col1/db-metadata.txt;
  fi
done < github-DBLinux.csv
