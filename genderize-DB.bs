#!/bin/bash

#PROXY: curl -x http://us-wa.proxymesh.com:31280 -U Mathew.Cathcart:PASSWORD https://api.genderize.io/?name=$FIRSTNAME

MetaDIR="../Database-Metadata/DB-metadata"
MetaFile="db-metadata.txt"
IDINPUT="API-LIMIT-EXCEEDED.txt"

DestDIR="genderize-DB-metadata"
DestFile="DB-Metadata.txt"

mkdir -p $DestDIR

while IFS=, read DB_NAME;
do
  mkdir -p $DestDIR/$DB_NAME
  NAMEGREPRESULT="$(grep -Po '"owner_name": *\K"[^"]*"' $MetaDIR/$DB_NAME/$MetaFile)";
  NAME="${NAMEGREPRESULT%\"}";
  NAME="${NAME#\"}";
  echo "DB-Name: $DB_NAME" >> $DestDIR/$DB_NAME/$DestFile;
  echo "" >> $DestDIR/$DB_NAME/$DestFile;
  echo "NAME: $NAME" >> $DestDIR/$DB_NAME/$DestFile;
  echo "" >> $DestDIR/$DB_NAME/$DestFile;
  curl https://api.genderize.io/?name=$NAME >> $DestDIR/$DB_NAME/$DestFile;
done < $IDINPUT