#!/bin/bash

#PROXY: curl -x http://us-wa.proxymesh.com:31280 -U Mathew.Cathcart:PASSWORD https://api.genderize.io/?name=$FIRSTNAME

MetaDIR="../Database-Metadata/DB-metadata"
MetaFile="db-metadata.txt"
IDINPUT="API-LIMIT-EXCEEDED.txt"

DestDIR="genderize-DB-metadata"
DestFile="DB-Metadata.txt"

mkdir -p $DestDIR

while IFS=, read DB_NAME;
do
  rm -rf $DestDIR/$DB_NAME;
done < $IDINPUT