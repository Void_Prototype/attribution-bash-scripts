#!/bin/bash

CSVDir="./";
CSVFile="java-fork-parsed_diversity_data.csv";
CSV="$CSVDir$CSVFile";

OutputDir="./"
OutputFile="gender-parsed_diversity_data.csv"
Output="$OutputDir$OutputFile"

head -1 $CSV >> $Output;

State1="$(head -1 $CSV | cut -d';' -f27)";
State2="$(head -1 $CSV | cut -d';' -f38)";

while IFS=, read -r col1
do
  #echo "Checking $col1"
  #echo "";
  ForkResult1="$(cut -d';' -f27 <<<"$col1")";
  echo "$State1 == $ForkResult1";
  if [ "$ForkResult1" == "female" ]; then
    #echo "Contains gender == 'female'. Checking blau_gender == 0";
    #echo "";
    ForkResult2="$(cut -d';' -f38 <<<"$col1")";
    echo "$State2 == $ForkResult2";
    echo 
    if [ "$ForkResult2" -eq "0" ]; then
      echo "Success. Adding result to parsed CSV file $Output";
      echo $col1 >> $Output;
      echo "";
    fi
  fi
done < $CSV