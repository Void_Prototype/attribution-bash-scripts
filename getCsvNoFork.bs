#!/bin/bash

CSVDir="./";
CSVFile="java-parsed_diversity_data.csv";
CSV="$CSVDir$CSVFile";

OutputDir="./"
OutputFile="java-fork-parsed_diversity_data.csv"
Output="$OutputDir$OutputFile"

head -1 $CSV >> $Output;

while IFS=, read -r col1
do
  echo "Checking $col1"
  echo "";
  ForkResult="$(cut -d';' -f8 <<<"$col1")";
  
  if [ "$ForkResult" -eq "0" ]; then
    echo "Success. Adding result to parsed CSV file $Output";
    echo $col1 >> $Output;
    echo "";
  fi
  echo $forkResult;
done < $CSV