# Attribution Scripts

Random scripts I made when working as a research student.
These scripts were all ad-hoc. I made each one to solve a task at the current time.

These scripts are not scalable, robust, or even commented well.

I also cannot remember what all of these scripts achieve in terms of functionality.