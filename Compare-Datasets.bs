#!/bin/bash

File1Name="female_dataset-logins.txt";
File1DIR="./";

File2Name="New-Female-Dataset.txt";
File2Dir="./";

FILE1="$File1DIR$File1Name";
FILE2="$File2DIR$File2Name";

File1Delim=",";
File2Delim=",";

Output="results.txt";

while IFS=$File1Delim read name1; do
  while IFS=$File2Delim read name2; do
    echo "$name1 --- $name2";
    if [ "$name1" == "$name2" ]; then
      echo "------------$name1 is a copy------------";
      echo $name1 >> $Output;
    fi
    sleep .4;
  done < $FILE2
done < $FILE1