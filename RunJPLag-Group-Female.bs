#!/bin/bash

FemaleDataset="./Repo-Female-Dataset"
ResultsDIR="./Results-Female-Group-JPLAG-Extracted"
JPLAGOutputDIR="./Results-Female-Group-JPLAG-Extra"
JPLAGVER="2.11.9"
JPLAG="jplag-$JPLAGVER.jar"
JavaVer="java17"

mkdir $ResultsDIR;
mkdir $JPLAGOutputDIR;

for DIR in $FemaleDataset/*; do
  for SUBDIR in $DIR/*; do
    for filename1 in $SUBDIR/*.java; do
      [ -e "$filename1" ] || continue;
      for DIR2 in $FemaleDataset/*; do
        
        if [ "$DIR" == "$DIR2" ]; then
          break;
        fi

        DIRNAME=$(basename "$DIR");
        DIRNAME2=$(basename "$DIR2");
        DIRNAME1="$DIRNAME-$DIRNAME2";
        mkdir $ResultsDIR/$DIRNAME1;
        mkdir $JPLAGOutputDIR/$DIRNAME1;

        for SUBDIR2 in $DIR2/*; do
          for filename2 in $SUBDIR2/*.java; do
            [ -e "$filename2" ] || continue;
            java -jar $JPLAG -l $JavaVer -r $JPLAGOutputDIR/$DIRNAME1 $filename1 $filename2 > $ResultsDIR/$DIRNAME1/Results.txt;
          done
        done
      done
    done
  done
done