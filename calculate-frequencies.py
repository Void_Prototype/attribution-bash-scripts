import os
from collections import OrderedDict

valuesfile="./6Gram-Metadata/6Gram-values.txt"
targetsfile="./6Gram-Metadata/6Gram-targets.txt"

# read samples from values.txt into an Ordered Dict.
# each dict key is a line from the file
# (including the trailing newline, but that doesn't matter)
# each dict value is 0

with open(valuesfile, 'r') as f:
  samplecount0=OrderedDict((sample, 0) for sample in f.readlines())

# get list of filenames from targets.txt

with open(targetsfile, 'r') as f:
  targets=[t.rstrip('\n') for t in f.readlines()]

# for each target,
# read its lines of samples
# increment the corresponding count in samplecount
# print out samplecount in a single line separated by commas
# each line also has the 2nd-to-last directory component of the target's pathname

for target in targets:
  with open(target, 'r') as f:
    # copy samplecount0 to samplecount so we don't have to read the values.txt file again
    samplecount=samplecount0.copy()
    # for each sample in the target file, increment the samplecount dict entry
    for tsample in f.readlines():
      #print(tsample)
      samplecount[tsample] += 1
    output = ','.join(str(v) for v in samplecount.values())
    output += ',' + os.path.basename(os.path.dirname(os.path.dirname(target)))
    print(output)
