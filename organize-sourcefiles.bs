#!/bin/bash

for JAVAPATH in $(find -name \*.java); do
  SUBPATH1="."
  SUBPATH2=$(cut -d/ -f2 <<< $JAVAPATH);
  SUBPATH3=$(cut -d/ -f3 <<< $JAVAPATH);
  DESTPATH="$SUBPATH1/$SUBPATH2/$SUBPATH3";
  cp $JAVAPATH $DESTPATH;
done