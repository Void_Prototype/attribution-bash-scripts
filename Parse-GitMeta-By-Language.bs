#!/bin/bash

MetaFile="Repo-Data.txt";
Language="Java";
RepoMetadataOutput="Repos.txt";
MetadataPDIR="./Repo-Data";

LoginLine="";
RepoNameLine="";
LangLine="";
HasForks="true";

echo "Running...";
for Dir in $MetadataPDIR/*/ ; do
  while read Line; do
    if [[ $Line == *"\"name\":"* ]]; then
      RepoNameLine=$Line;
    fi
    if [[ $Line == *"\"login\":"* ]]; then
      LoginLine=$Line;
    fi
    if [[ $Line == *"\"forks\": 0"* ]]; then
      HasForks="false";
    fi
    if [[ $Line == *"\"language\":"* ]]; then
      if [[ $Line == *"\"language\": \"Java\""* ]]; then
        if [[ $HasForks == "false" ]]; then
          LangLine=$Line;
          GrepRepoName="$(echo $RepoNameLine | grep -Po '"name": *\K"[^"]*"')";
          GrepLoginName="$(echo $LoginLine | grep -Po '"login": *\K"[^"]*"')";
          RepoName="${GrepRepoName%\"}";
          LoginName="${GrepLoginName%\"}";
          RepoName="${RepoName#\"}";
          LoginName="${LoginName#\"}";
          echo "{\"username\":\"$LoginName\",\"repo-name\":\"$RepoName\",\"language\":\"java\"}" >> $RepoMetadataOutput;
          echo "Computing $LoginName --- $RepoName";
        fi
      fi
      HasForks="true";
    fi
  done < $Dir/$MetaFile
done