#!/bin/bash

INPUT="./female_dataset-logins.txt"

OAUTH_TOKEN=$(<Oauth_Token.txt)
echo "$OAUTH_TOKEN"

mkdir -p git_metadata

while IFS=, read ID;
do
  mkdir -p git_metadata/$ID;
  curl -H "Authorization: token $OAUTH_TOKEN" https://api.github.com/users/$ID > git_metadata/$ID/metadata-git.txt;
  sleep 20s;
done < $INPUT
