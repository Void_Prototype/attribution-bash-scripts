#!/bin/bash

IDINPUT="../id-all.txt"
OUTPUT="genderize-results.txt"
METADIR="./genderize-metadata"
METAINPUT="metadata-genderize.txt"

touch $OUTPUT

while IFS=, read ID;
do
  NAMEGREPRESULT="$(grep -Po '"name": *\K"[^"]*"' $METADIR/$ID/$METAINPUT)";
  GENDERGREPRESULT="$(grep -Po '"gender": *\K"[^"]*"' $METADIR/$ID/$METAINPUT)";
  NAME="${NAMEGREPRESULT%\"}";
  NAME="${NAME#\"}";
  GENDER="${GENDERGREPRESULT%\"}";
  GENDER="${GENDER#\"}";
  echo "ID: $ID --- NAME: $NAME --- GENDER: $GENDER" >> $OUTPUT;
done < $IDINPUT
