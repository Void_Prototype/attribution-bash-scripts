#!/bin/bash

MetaDIR="./genderize-DB-metadata"
MetaFile="DB-metadata.txt"
IDINPUT="../Database-Metadata/DB-repo_names.txt"

DestDIR="./"
DestFile="API-LIMIT-EXCEEDED.txt"

while IFS=, read DB_NAME;
do
  ERRORGREPRESULT="$(grep -Po '"error": *\K"[^"]*"' $MetaDIR/$DB_NAME/$MetaFile)";
  ERROR="${ERRORGREPRESULT%\"}";
  ERROR="${ERROR#\"}";
  if [[ "$ERROR" == "Request limit reached" ]]; then
    echo "$DB_NAME," >> $DestDIR/$DestFile;
  fi
 
done < $IDINPUT

