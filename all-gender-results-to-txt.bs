#!/bin/bash

IDINPUT1="./id-all.txt"
IDINPUT2="./DB-repo_names.txt"

FEMALEOUTPUT="genderize-femaleresults.txt"
MALEOUTPUT="genderize-maleresults.txt"
NULLGENDEROUTPUT="genderize-nullresults.txt"

METADIR1="./genderize-metadata"
METADIR2="./genderize-DB-metadata"

METAINPUT1="metadata-genderize.txt"
METAINPUT2="DB-Metadata.txt"

touch $FEMALEOUTPUT
touch $MALEOUTPUT
touch $NULLGENDEROUTPUT

while IFS=, read ID;
do
  NAMEGREPRESULT="$(grep -Po '"name": *\K"[^"]*"' $METADIR1/$ID/$METAINPUT1)";
  GENDERGREPRESULT="$(grep -Po '"gender": *\K"[^"]*"' $METADIR1/$ID/$METAINPUT1)";
  NAME="${NAMEGREPRESULT%\"}";
  NAME="${NAME#\"}";
  GENDER="${GENDERGREPRESULT%\"}";
  GENDER="${GENDER#\"}";
  if [[ "$GENDER" == "male" ]]; then
    echo "{\"name\":\"$NAME\",\"gender\":\"$GENDER\",\"github-login\":\"$ID\"}" >> $MALEOUTPUT;
  elif [[ "$GENDER" == "female" ]]; then
    echo "{\"name\":\"$NAME\",\"gender\":\"$GENDER\",\"github-login\":\"$ID\"}" >> $FEMALEOUTPUT;
  else
    echo "{\"name\":\"$NAME\",\"gender\":\"$GENDER\",\"github-login\":\"$ID\"}" >> $NULLGENDEROUTPUT;
  fi
done < $IDINPUT1


while IFS=, read ID;
do
  NAMEGREPRESULT="$(grep -Po '"name": *\K"[^"]*"' $METADIR2/$ID/$METAINPUT2)";
  GENDERGREPRESULT="$(grep -Po '"gender": *\K"[^"]*"' $METADIR2/$ID/$METAINPUT2)";
  NAME="${NAMEGREPRESULT%\"}";
  NAME="${NAME#\"}";
  GENDER="${GENDERGREPRESULT%\"}";
  GENDER="${GENDER#\"}";
  if [[ "$GENDER" == "male" ]]; then
    echo "{\"name\":\"$NAME\",\"gender\":\"$GENDER\",\"repo_name\":\"$ID\"}" >> $MALEOUTPUT;
  elif [[ "$GENDER" == "female" ]]; then
    echo "{\"name\":\"$NAME\",\"gender\":\"$GENDER\",\"repo_name\":\"$ID\"}" >> $FEMALEOUTPUT;
  else
    echo "{\"name\":\"$NAME\",\"gender\":\"$GENDER\",\"repo_name\":\"$ID\"}" >> $NULLGENDEROUTPUT;
  fi
done < $IDINPUT2