#!/bin/bash

ext="java";
DatasetDIR="./dataset";
TmpBiggestFile="biggest-file.txt";
TmpBiggestSize="biggest-filesize.txt";

touch $TmpBiggestFile;
touch $TmpBiggestSize;

echo "0" > $TmpBiggestSize;
echo "" > $TmpBiggestFile;

for F in $DatasetDIR/*/*/*.$ext; do
    Largest=$(cat $TmpBiggestSize);
    Filesize=$(wc -c "$F" | cut -f1 -d' ');

    echo "Largest == $Largest";
    echo "Filesize == $Filesize";

    if [ "$Filesize" -gt "$Largest" ]; then
        echo "-----NEW LARGEST FOUND-----";
        echo "$Filesize" > $TmpBiggestSize;
        echo "$F" > $TmpBiggestFile;
    fi
done