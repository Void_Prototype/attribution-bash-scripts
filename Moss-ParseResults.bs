#!/bin/bash

#----USAGE----

#------------


#----Modifiable Variables----
Language="java";
InputDIR="./MossTest";
declare -i Depth=3;
declare -i Mode=1;
#------------


#----Un Modifiable Variables----
ResultsDIR="MossResults";
declare -i i=1;
declare -i j=1;
#------------


#----Parse Results----
ParseResult1="";
ParseResult2="";
ParseDir1="";
ParseDir2="";
FileName1="";
FileName2="";
grep -o "./MossTest.*" Result-Content.txt | while read -r line ;
do
  if [ "$j" -eq "1" ]; then
    ParseResult1=$(echo "$line" | grep -oE '[0-9]\%|[0-9][0-9]\%|100\%');
    ParseDir1=$(echo "$line" | grep -o './MossTest.*.java');
    FileName1=${ParseDir1%%+(/)};
    FileName1=${FileName1##*/};
    echo "Result1==$ParseResult1";
    echo "Dir1==$ParseDir1";
    echo "FileName1==$FileName1";
    j=2;
  elif [ "$j" -eq "2" ]; then
    ParseResult2=$(echo "$line" | grep -oE '[0-9]\%|[0-9][0-9]\%|100\%');
    ParseDir2=$(echo "$line" | grep -o './MossTest.*.java');
    FileName2=${ParseDir1%%+(/)};
    FileName2=${FileName1##*/};
    echo "Result2==$ParseResult2";
    echo "Dir2==$ParseDir2";
    echo "FileName2==$FileName2";
    j=1;
  else
    echo "Fatal Error. j == $j";
    exit 1;
  fi
  sleep 1s;
done
#------------