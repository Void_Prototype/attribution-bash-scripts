#!/bin/bash

FemaleDataset="./Repo-Female-Dataset"
ResultsDIR="./Results-Female-Instance-JPLAG-Extracted"
JPLAGOutputDIR="./Results-Female-Instance-JPLAG-Extra"
JPLAGVER="2.11.9"
JPLAG="jplag-$JPLAGVER.jar"
JavaVer="java17"

mkdir $JPLAGOutputDIR;
mkdir $ResultsDIR;

for DIR in $FemaleDataset/*; do
  DIRNAME=$(basename "$DIR");
  mkdir $ResultsDIR/$DIRNAME;
  mkdir $JPLAGOutputDIR/$DIRNAME;
  for SUBDIR in $DIR/*; do
    java -jar $JPLAG -l $JavaVer -r $JPLAGOutputDIR/$DIRNAME $SUBDIR > $ResultsDIR/$DIRNAME/Results.txt
  done
done