#!/bin/bash

Gender="Female";
MossDIR="../../Data-Extraction/Moss-Extraction";
DatasetDIR=".";

MossResults="$MossDIR/$Gender-MossResults/$Gender-MossResults.txt";
Dataset="$DatasetDIR/$Gender-Dataset-Filtered";

TmpDIR="TmpDir";
CacheFile="$TmpDIR/cachefile.txt";
VarFile1="$TmpDIR/varfile1.txt";
VarFile2="$TmpDIR/varfile2.txt";
KillList="$TmpDIR/killlist.txt";

NewMossList="$MossDIR/$Gender-MossResults/$Gender-MossResults-TMP.txt";
touch $NewMossList;

#SET THRESHOLD
declare -i Threshold=90;

if [ -d "$TmpDIR" ]; then
  rm -rf $TmpDIR;
fi

mkdir $TmpDIR;
touch $CacheFile;
touch $KillList;
touch $VarFile1;
touch $VarFile2;

declare -i tmpVar=0;
echo $tmpVar > tmpVar.txt;

while read MossResultLine; do
  echo "----------------------------------------";
  echo "Line == '$MossResultLine'";
  echo "0" > $VarFile1;
  echo "0" > $VarFile2;
  FILE1Grep="$(echo $MossResultLine | grep -Po '"File1":*\K"[^"]*"')";
  FILE1="${FILE1Grep%\"}";
  FILE1="${FILE1#\"}";

  FILE2Grep="$(echo $MossResultLine | grep -Po '"File2":*\K"[^"]*"')";
  FILE2="${FILE2Grep%\"}";
  FILE2="${FILE2#\"}";

  RESULTGrep="$(echo $MossResultLine | grep -Po '"Result":*\K"[^"]*"')";
  RESULT="${RESULTGrep%\"}";
  RESULT="${RESULT#\"}";
  RESULT=$(echo $RESULT | sed 's/%//');

  echo "FILE1 == '$FILE1'";
  echo "FILE2 == '$FILE2'";
  echo "RESULT == '$RESULT'";
  echo "";
  
  if [ "$RESULT" -ge "$Threshold" ]; then
    echo "RESULT '$RESULT' >= Threshold '$Threshold'";
    while read CacheLine; do
      if [[ $CacheLine == *"$FILE1"* ]]; then
        echo "1" > $VarFile1;
      fi
      if [[ $CacheLine == *"$FILE2"* ]]; then
        echo "1" > $VarFile2;
      fi
    done < $CacheFile

    Flag1=$(cat $VarFile1);
    Flag2=$(cat $VarFile2);

    if [ "$Flag1" -eq "0" ]; then
      echo "FILE1 NOT FOUND in cache.";
      echo $FILE1 >> $CacheFile;
      echo $FILE1 >> $KillList;
      rm -rf $FILE1;
    elif [ "$Flag1" -eq "1" ]; then
      echo "FILE1 FOUND in cache.";
    else
      echo "FILE1 -- ERROR----ERROR----ERROR";
    fi

    if [ "$Flag2" -eq "0" ]; then
      echo "FILE2 NOT FOUND in cache.";
      echo $FILE2 >> $CacheFile;
    elif [ "$Flag2" -eq "1" ]; then
      echo "FILE2 FOUND in cache.";
    else
      echo "FILE2 -- ERROR----ERROR----ERROR";
    fi
  else
    echo "RESULT '$RESULT' < Threshold '$Threshold'";
  fi
  echo "----------------------------------------";
done < $MossResults

while read MossResultsLine2; do
  echo "------------------------------";
  echo "0" > tmpVar.txt;
  while read KillListLine; do
  echo "----------";
  comparable=$(cat ./tmpVar.txt);
  echo "MossResultsLine2 == '$MossResultsLine2'";
  echo "KillListLine == $KillListLine";
  echo "Comparable == '$comparable'";

  if [[ $MossResultsLine2 != *"$KillListLine"* ]]; then
    echo "MossResultsLine2 DOES NOT CONTAIN KillListLine";
    if [ "$comparable" -eq "0" ]; then
      echo "++ADDING MossResultsLine2 to NewMossList";
      echo $MossResultsLine2 >> $NewMossList;
      echo "1" > tmpVar.txt;
    else
      echo "--NOT adding to NewMossList";
    fi
  else
    echo "MossResults CONTAINS KillListLine";
  fi
  done < $KillList
  echo "------------------------------";
done < $MossResults

if [ -s $KillList ]; then
  
  while read target; do
    echo "Removing '$target'";
    rm $target;
  done < $KillList
  echo "DONE";
fi

rm -rf $TmpDIR;
rm -rf tmpVar.txt;
rm $MossResults;
cp $NewMossList $MossResults;
rm $NewMossList;
./Moss-FileFilter.bs
