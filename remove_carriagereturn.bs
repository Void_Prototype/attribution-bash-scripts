#!/bin/bash

Directory="Modified-Dataset";
Ext="java";

find $Directory -type f -name "*.$Ext" | while IFS= read file_name; do
    #grep -U $'\x0D' $file_name;
    sed -i 's/\r$//g' $file_name;
done