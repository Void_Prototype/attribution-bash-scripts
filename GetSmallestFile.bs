#!/bin/bash

ext="java";
DatasetDIR="./dataset";
TmpSmallestFile="smallest-file.txt";
TmpSmallestSize="smallest-filesize.txt";

touch $TmpSmallestFile;
touch $TmpSmallestSize;

echo "0" > $TmpSmallestSize;
echo "" > $TmpSmallestFile;

for F in $DatasetDIR/*/*/*.$ext; do
    Smallest=$(cat $TmpSmallestSize);
    Filesize=$(wc -c "$F" | cut -f1 -d' ');

    echo "Smallest == $Smallest";
    echo "Filesize == $Filesize";
    
    if [ "$Smallest" -eq "0" ]; then
        echo "$Filesize" > $TmpSmallestSize;
        echo "$F" > $TmpSmallestFile;
    elif [ "$Filesize" -lt "$Smallest" ]; then
        echo "-----NEW Smallest FOUND-----";
        echo "$Filesize" > $TmpSmallestSize;
        echo "$F" > $TmpSmallestFile;
    fi
done