#!/bin/bash

OutputDirectory="./6Gram-Metadata";
OutputName="6Gram-Frequencies";
OutputExtension="txt";

measure_start() {
    declare -g ttic start;
    echo "==> Start $* <==";
    ttic_start=$(date +%s.%N);
}

measure_end() {
    local end;
    end=$(date +%s.%N);
    local start;
    start="$ttic_start";
    ttic_runtime=$(python -c "print(${end} - ${start})");
    echo "Runtime: $ttic_runtime";
    echo;
}


measure_start Python Frequency Calculator
    python3 calculate-frequencies.py >> $OutputDirectory/$OutputName.$OutputExtension;
measure_end