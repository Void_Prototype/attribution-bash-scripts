#!/bin/bash

for DIR in *; do
  if [ -d "${DIR}" ]; then
    (
      cd "$DIR";
      for SUBDIR in *; do
        if [ -d "$SUBDIR" ]; then
        (
          cd "$SUBDIR";
          rm -rf ./*/;
          rm -rf .git;
	        find . -type f ! -name '*.java' -delete;
        )
        fi
      done
    );
  fi
done
