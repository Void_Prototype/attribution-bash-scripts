#!/bin/bash

OldDatasetDIR="./Repo-Old-Female-Dataset"
NewDatasetDIR="./Repo-New-Female-Dataset"

ValidFemaleList="./New-Female-Dataset.txt"

mkdir $NewDatasetDIR;

while IFS=, read ID;
do
	mkdir $NewDatasetDIR/$ID;
	cp -r $OldDatasetDIR/$ID $NewDatasetDIR;
done < $ValidFemaleList
