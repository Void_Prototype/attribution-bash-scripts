#!/bin/bash

OAUTH_TOKEN=$(<Oauth_Token.txt)
OUTPUTDIR="Repo-Female-Dataset";
INPUTREPONAMES="FEMALE-id-all.txt"
DBMETADATA="../Database-Metadata/DB-metadata/"
mkdir -p $OUTPUTDIR;

while IFS=, read REPONAME;
do
  GREPNAME=$(grep -Po '"owner_name": *\K"[^"]*"' <<< $(<$DBMETADATA/$REPONAME/db-metadata.txt));
  NAME="${GREPNAME%\"}";
  NAME="${NAME#\"}";
  while read line;
  do
    LOGINGREPRESULT=$(grep -Po '"login": *\K"[^"]*"' <<< $line);
    LOGIN="${LOGINGREPRESULT%\"}";
    LOGIN="${LOGIN#\"}";
    if [ -z "$LOGIN" ]
    then
      :
    else
      CURLOUTPUT=$(curl -H "Authorization: token $OAUTH_TOKEN" https://api.github.com/users/$LOGIN);
      GREPCHECKNAME=$(grep -Po '"name": *\K"[^"]*"' <<< $CURLOUTPUT);
      CHECKNAME="${GREPCHECKNAME%\"}";
      CHECKNAME="${CHECKNAME#\"}";
      if [[ "$CHECKNAME" == *"$NAME"* ]]
      then
        mkdir -p $OUTPUTDIR/$LOGIN/$REPONAME;
        git clone https://github.com/$LOGIN/$REPONAME.git $OUTPUTDIR/$LOGIN/$REPONAME;
        break;
      else
        :
      fi
    fi
  done < ./REPONAME-METADATA/$REPONAME/repo-meta.txt
  sleep 1s;
done < $INPUTREPONAMES