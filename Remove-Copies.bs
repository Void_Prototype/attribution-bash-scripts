#!/bin/bash

FileName="female_dataset-logins.txt";
FileDIR="./";

FILE="$FileDIR$FileName";

File1Delim=",";
File2Delim=",";

Output="copies.txt";

Counter=0;
CompVal=1;

while IFS=$File1Delim read name1; do
  Counter=0;
  while IFS=$File2Delim read name2; do
    echo "$name1 --- $name2";
    if [ "$name1" == "$name2" ]; then
      #Increment counter
      Counter=$[$Counter + 1];

      if [ "$Counter" -gt "$CompVal" ]; then
        echo "------------$name1 is a copy------------";
        echo $name1 >> $Output;
      fi
    fi
    echo "+++++++++++++++     $Counter     ++++++++++++++++";
  done < $FILE
done < $FILE