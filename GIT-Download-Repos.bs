#!/bin/bash

ReponameFile="./Repos.txt";
ResultsDIR="Female-Git-Repos";
#GIT-Oauth-Token=$(<Oauth_Token.txt);

echo "Running...";

if [ -d "$ResultsDIR" ]; then
  rm -rf $ResultsDIR;
fi

mkdir $ResultsDIR;

while read Line; do
  GrepUsername="$(echo $Line | grep -Po '"username":*\K"[^"]*"')";
  GrepReponame="$(echo $Line | grep -Po '"repo-name":*\K"[^"]*"')";
  GrepLang="$(echo $Line | grep -Po '"language":*\K"[^"]*"')";

  Username="${GrepUsername%\"}";
  Reponame="${GrepReponame%\"}";
  Lang="${GrepLang%\"}";


  Username="${Username#\"}";
  Reponame="${Reponame#\"}";
  Lang="${Lang#\"}";

  if [ ! -d "$ResultsDIR/$Username" ]; then
    mkdir -p $ResultsDIR/$Username;
  fi

  git clone https://github.com/$Username/$Reponame ./$ResultsDIR/$Username/$Reponame;

done < $ReponameFile

