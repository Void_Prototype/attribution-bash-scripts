#!/bin/bash

CSVDir="./";
CSVFile="diversity_data.csv";
CSV="$CSVDir$CSVFile";

OutputDir="./"
OutputFile="java-parsed_diversity_data.csv"
Output="$OutputDir$OutputFile"

head -1 $CSV >> $Output;

while IFS=, read -r col1
do
  if [[ $col1 == *";Java;"* ]]; then
    echo "Adding $col1";
    echo $col1 >> $Output;
    echo "";
  fi
done < $CSV