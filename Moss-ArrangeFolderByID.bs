#!/bin/bash

Gender="Female";
IDInput="./id-all-$Gender.txt";
DestinationDIR="./$Gender-Dataset-Filtered";

while IFS= read -r line; do
  mkdir -p $line;
  for dir in $DestinationDIR/*; do
    echo "DIR == $dir";
    echo "LINE == $line";
    if [[ $dir == *"$line"* ]]; then
      for file in $dir/*.java; do
        mv $file $line;
      done
      rm -rf $dir;
    fi   
  done
  mv $line $DestinationDIR;
done < $IDInput
