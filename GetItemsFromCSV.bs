#!/bin/bash

CSVDir="./";
CSVFile="female_dataset.csv";
CSV="$CSVDir$CSVFile";

OutputDir="./"
OutputFile="female_dataset-logins.txt"
Output="$OutputDir$OutputFile"

Delimiter=","

while IFS=, read -r col1 col2 col3 col4 col5
do
  echo "$col3$Delimiter" >> $Output;
done < $CSV