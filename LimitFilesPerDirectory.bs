#!/bin/bash

Directory="./50-50-Dataset";
PermittedFiles="40";

find $Directory -type d | while read dir; do
  FoundFiles=$(ls -p $dir | grep -v / | wc -l);
  if [ "$FoundFiles" -gt "$PermittedFiles" ]; then
    echo "NumFound == $FoundFiles";
    ls $dir | sort -R | tail -$PermittedFiles > keepfiles.txt;
    ls $dir | while read file; do
      if ! grep -q $file keepfiles.txt; then
        #echo "File == $dir/$file";
        rm -f $dir/$file;
      fi
    done
  fi
done

rm -f keepfiles.txt;