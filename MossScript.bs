#!/bin/bash

#----USAGE----

#------------


#----Modifiable Variables----
Gender="Female";
Language="java";
InputDIR="./$Gender-Dataset-Filtered";
ResultsDIR="$Gender-MossWebResults";
ResultsDIR2="$Gender-MOSSExtractedResults";
ResultsTXT="$Gender-MossResults.txt";
CounterResults="$Gender-CounterResults.txt";
MossResultsDIR="../../Data-Extraction/Moss-Extraction/$Gender-MossResults";
declare -i Depth=2;
declare -i Mode=1;
#------------

#----Un Modifiable Variables----
declare -i i=1;
declare -i j=1;
ParseResult1="";
ParseResult2="";
declare -i ParseNum1=0;
declare -i ParseNum2=0;
ParseDir1="";
ParseDir2="";
FileName1="";
FileName2="";
ResultFile0="Moss_0-10";
ResultFile1="Moss_0-20";
ResultFile2="Moss_0-30";
ResultFile3="Moss_0-40";
ResultFile4="Moss_0-50";
ResultFile5="Moss_0-60";
ResultFile6="Moss_0-70";
ResultFile7="Moss_0-80";
ResultFile8="Moss_0-90";
ResultFile9="Moss_0-100";
declare -i ZeroCTR=0;
declare -i OneCTR=0;
declare -i TwoCTR=0;
declare -i ThreeCTR=0;
declare -i FourCTR=0;
declare -i FiveCTR=0;
declare -i SixCTR=0;
declare -i SevenCTR=0;
declare -i EightCTR=0;
declare -i NineCTR=0;
#------------

#----Setup Temporary Variable Files to deal with Subshell Problem----
echo "------Creating Temporary Var Files------";
echo "$ZeroCTR" > zero-tmp.txt;
echo "$OneCTR" > one-tmp.txt;
echo "$TwoCTR" > two-tmp.txt;
echo "$ThreeCTR" > three-tmp.txt;
echo "$FourCTR" > four-tmp.txt;
echo "$FiveCTR" > five-tmp.txt;
echo "$SixCTR" > six-tmp.txt;
echo "$SevenCTR" > seven-tmp.txt;
echo "$EightCTR" > eight-tmp.txt;
echo "$NineCTR" > nine-tmp.txt;
echo "------Done Creating Temp Var Files------";
#------------

echo "";

#----Setup Moss User Directories----
if [ -f "$ResultsTXT" ]; then
  echo "Results TXT file exists. Removing";
  rm -rf $ResultsTXT;
fi
if [ -f "$CounterResults" ]; then
  echo "Counter Results TXT file exists. Removing";
  rm -rf $CounterResults;
fi
echo "------Setting Up Results Directories------";
if [ -d "$MossResultsDIR" ]; then
  echo "Directory '$MossResultsDIR' exists. Re-Creating Directory";
  sleep 3s;
  echo "Recursively deleting directory '$MossResultsDIR'";
  rm -rf $MossResultsDIR;
fi
echo "Creating new directory '$MossResultsDIR'";
mkdir $MossResultsDIR;

if [ -d "$ResultsDIR" ]; then
  echo "Directory '$ResultsDIR' exists. Re-Creating Directory";
  sleep 3s;
  echo "Recursively deleting directory '$ResultsDIR'";
  rm -rf $ResultsDIR;
fi
echo "Creating new directory '$ResultsDIR'";
mkdir $ResultsDIR;
echo "Creating user directories";
find $InputDIR -type d > directories.txt;
while read Directory;
do
  UserDIR=$(echo ${Directory#$InputDIR/});
  mkdir -p ./$ResultsDIR/$UserDIR;
done < directories.txt
rm -rf directories.txt;
echo "------Done Creating Moss User Directories------";
#------------

echo "";

#----Setup Results Directories----
echo "------Setting Up Results Directories------";
if [ -d "$ResultsDIR2" ]; then
  echo "Directory '$ResultsDIR2' exists. Re-Creating Directory";
  sleep 3s;
  echo "Recursively deleting directory '$ResultsDIR2'";
  rm -rf $ResultsDIR2;
fi
echo "Creating new directory '$ResultsDIR2'";
mkdir $ResultsDIR2;
mkdir -p ./$ResultsDIR2/$ResultFile0;
mkdir -p ./$ResultsDIR2/$ResultFile1;
mkdir -p ./$ResultsDIR2/$ResultFile2;
mkdir -p ./$ResultsDIR2/$ResultFile3;
mkdir -p ./$ResultsDIR2/$ResultFile4;
mkdir -p ./$ResultsDIR2/$ResultFile5;
mkdir -p ./$ResultsDIR2/$ResultFile6;
mkdir -p ./$ResultsDIR2/$ResultFile7;
mkdir -p ./$ResultsDIR2/$ResultFile8;
mkdir -p ./$ResultsDIR2/$ResultFile9;
echo "------Done Creating Results Directories------";
#------------

#----Moss Requests----
DepthSubDIR="";
while [ $i -lt $Depth ];
do
  DepthSubDIR="$DepthSubDIR/*"
  i=$[$i+1];
done
i=0;

echo "------Sending Moss Requests And Getting Results------";
echo $InputDIR$DepthSubDIR > userDirectories.txt;
if [ "$Mode" -eq "1" ]; then
  echo "File Comparison Mode";
  while IFS=' ' read -ra Directory; do
    for d in "${Directory[@]}"; do
      TargetSubDIR=$(echo ${d#$InputDIR/});
      TargetDIR="$ResultsDIR/$TargetSubDIR";
      echo "TARGET SUBDIR ====== $TargetSubDIR";
      OutputURL=$(./moss -l $Language $d/*.$Language | tail -1);
      WGetResult=$(wget -qO- $OutputURL);
      echo "$OutputURL" > $TargetDIR/Result-URL.txt;
      echo "$WGetResult" > $TargetDIR/Result-Content.txt;

      #----Parse Moss Results----
      echo "------Parsing Moss Results------";
      grep -o "$InputDIR.*" $TargetDIR/Result-Content.txt | while read -r line ;
      do
        if [ "$j" -eq "1" ]; then
          ParseResult1=$(echo "$line" | grep -oE '[0-9]\%|[0-9][0-9]\%|100\%');
          ParseDir1=$(echo "$line" | grep -o "$InputDIR.*.java");
          FileName1=${ParseDir1%%+(/)};
          FileName1=${FileName1##*/};
          echo "Result1==$ParseResult1";
          echo "Dir1==$ParseDir1";
          echo "FileName1==$FileName1";
          j=2;
        elif [ "$j" -eq "2" ]; then
          ParseResult2=$(echo "$line" | grep -oE '[0-9]\%|[0-9][0-9]\%|100\%');
          ParseDir2=$(echo "$line" | grep -o "$InputDIR.*.java");
          FileName2=${ParseDir2%%+(/)};
          FileName2=${FileName2##*/};
          echo "Result2==$ParseResult2";
          echo "Dir2==$ParseDir2";
          echo "FileName2==$FileName2";
          echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsTXT;
          echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsTXT;
          j=1;
          #----Put Results in respective Subdirectories----
          ParseNum1=$(echo $ParseResult1 | sed 's/%//');
          ParseNum2=$(echo $ParseResult2 | sed 's/%//');
        
          if [ "$ParseNum1" -le "10" ]; then
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile0/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile1/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile2/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile3/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile4/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile5/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile6/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile7/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile8/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$ZeroCTR;
            ZeroCTR=$((NumPlace+1));
          elif [ "$ParseNum1" -le "20" ]; then
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile1/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile2/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile3/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile4/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile5/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile6/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile7/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile8/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$OneCTR;
            OneCTR=$((NumPlace+1));
          elif [ "$ParseNum1" -le "30" ]; then
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile2/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile3/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile4/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile5/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile6/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile7/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile8/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$TwoCTR;
            TwoCTR=$((NumPlace+1));
          elif [ "$ParseNum1" -le "40" ]; then
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile3/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile4/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile5/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile6/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile7/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile8/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$ThreeCTR;
            ThreeCTR=$((NumPlace+1));
          elif [ "$ParseNum1" -le "50" ]; then
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile4/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile5/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile6/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile7/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile8/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$FourCTR;
            FourCTR=$((NumPlace+1));
          elif [ "$ParseNum1" -le "60" ]; then
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile5/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile6/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile7/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile8/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$FiveCTR;
            FiveCTR=$((NumPlace+1));
          elif [ "$ParseNum1" -le "70" ]; then
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile6/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile7/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile8/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$SixCTR;
            SixCTR=$((NumPlace+1));
          elif [ "$ParseNum1" -le "80" ]; then
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile7/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile8/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$SevenCTR;
            SevenCTR=$((NumPlace+1));
          elif [ "$ParseNum1" -le "90" ]; then
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile8/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$EightCTR;
            EightCTR=$((NumPlace+1));
          elif [ "$ParseNum1" -le "100" ]; then
            echo "{\"File1\":\"$ParseDir1\",\"File2\":\"$ParseDir2\",\"Result\":\"$ParseResult1\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$NineCTR;
            NineCTR=$((NumPlace+1));
          fi

          if [ "$ParseNum2" -le "10" ]; then
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile0/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile1/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile2/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile3/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile4/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile5/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile6/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile7/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile8/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$ZeroCTR;
            ZeroCTR=$((NumPlace+1));
          elif [ "$ParseNum2" -le "20" ]; then
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile1/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile2/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile3/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile4/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile5/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile6/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile7/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile8/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$OneCTR;
            OneCTR=$((NumPlace+1));
          elif [ "$ParseNum2" -le "30" ]; then
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile2/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile3/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile4/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile5/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile6/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile7/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile8/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$TwoCTR;
            TwoCTR=$((NumPlace+1));
          elif [ "$ParseNum2" -le "40" ]; then
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile3/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile4/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile5/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile6/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile7/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile8/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$ThreeCTR;
            ThreeCTR=$((NumPlace+1));
          elif [ "$ParseNum2" -le "50" ]; then
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile4/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile5/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile6/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile7/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile8/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=FourCTR;
            FourCTR=$((NumPlace+1));
          elif [ "$ParseNum2" -le "60" ]; then
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile5/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile6/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile7/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile8/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$FiveCTR;
            FiveCTR=$((NumPlace+1));
          elif [ "$ParseNum2" -le "70" ]; then
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile6/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile7/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile8/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$SixCTR;
            SixCTR=$((NumPlace+1));
          elif [ "$ParseNum2" -le "80" ]; then
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile7/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile8/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$SevenCTR;
            SevenCTR=$((NumPlace+1));
          elif [ "$ParseNum2" -le "90" ]; then
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile8/$ResultsTXT;
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$EightCTR;
            EightCTR=$((NumPlace+1));
          elif [ "$ParseNum2" -le "100" ]; then
            echo "{\"File1\":\"$ParseDir2\",\"File2\":\"$ParseDir1\",\"Result\":\"$ParseResult2\"}" >> $ResultsDIR2/$ResultFile9/$ResultsTXT;
            NumPlace=$NineCTR;
            NineCTR=$((NumPlace+1));
          fi

          #------------
        else
          echo "Fatal Error. j == $j";
          exit 1;
        fi
        
        echo "$ZeroCTR" > ./zero-tmp.txt;
        echo "$OneCTR" > ./one-tmp.txt;
        echo "$TwoCTR" > ./two-tmp.txt;
        echo "$ThreeCTR" > ./three-tmp.txt;
        echo "$FourCTR" > ./four-tmp.txt;
        echo "$FiveCTR" > ./five-tmp.txt;
        echo "$SixCTR" > ./six-tmp.txt;
        echo "$SevenCTR" > ./seven-tmp.txt;
        echo "$EightCTR" > ./eight-tmp.txt;
        echo "$NineCTR" > ./nine-tmp.txt;

      done
      
      ZeroCTR=$(cat ./zero-tmp.txt);
      OneCTR=$(cat ./one-tmp.txt);
      TwoCTR=$(cat ./two-tmp.txt);
      ThreeCTR=$(cat ./three-tmp.txt);
      FourCTR=$(cat ./four-tmp.txt);
      FiveCTR=$(cat ./five-tmp.txt);
      SixCTR=$(cat ./six-tmp.txt);
      SevenCTR=$(cat ./seven-tmp.txt);
      EightCTR=$(cat ./eight-tmp.txt);
      NineCTR=$(cat ./nine-tmp.txt);

      echo "ZeroCTR == $ZeroCTR";
      echo "OneCTR == $OneCTR";
      echo "TwoCTR == $TwoCTR";
      echo "ThreeCTR == $ThreeCTR";
      echo "FourCTR == $FourCTR";
      echo "FiveCTR == $FiveCTR";
      echo "SixCTR == $SixCTR";
      echo "SevenCTR == $SevenCTR";
      echo "EightCTR == $EightCTR";
      echo "NineCTR == $NineCTR";   

      echo "------Done Parsing Moss Results------";
      #------------

    done
  done < userDirectories.txt;
  rm -rf userDirectories.txt;

  ZeroCTRMod=$ZeroCTR;
  OneCTRMod=$((ZeroCTR + OneCTR));
  TwoCTRMod=$((ZeroCTR + OneCTR + TwoCTR));
  ThreeCTRMod=$((ZeroCTR + OneCTR + TwoCTR + ThreeCTR));
  FourCTRMod=$((ZeroCTR + OneCTR + TwoCTR + ThreeCTR + FourCTR));
  FiveCTRMod=$((ZeroCTR + OneCTR + TwoCTR + ThreeCTR + FourCTR + FiveCTR));
  SixCTRMod=$((ZeroCTR + OneCTR + TwoCTR + ThreeCTR + FourCTR + FiveCTR + SixCTR));
  SevenCTRMod=$((ZeroCTR + OneCTR + TwoCTR + ThreeCTR + FourCTR + FiveCTR + SixCTR + SevenCTR));
  EightCTRMod=$((ZeroCTR + OneCTR + TwoCTR + ThreeCTR + FourCTR + FiveCTR + SixCTR + SevenCTR + EightCTR));
  NineCTRMod=$((ZeroCTR + OneCTR + TwoCTR + ThreeCTR + FourCTR + FiveCTR + SixCTR + SevenCTR + EightCTR + NineCTR));

  echo "{\"Thresh\":\"0-10\",\"Result\":\"$ZeroCTRMod\"}" >> $CounterResults;
  echo "{\"Thresh\":\"0-20\",\"Result\":\"$OneCTRMod\"}" >> $CounterResults;
  echo "{\"Thresh\":\"0-30\",\"Result\":\"$TwoCTRMod\"}" >> $CounterResults;
  echo "{\"Thresh\":\"0-40\",\"Result\":\"$ThreeCTRMod\"}" >> $CounterResults;
  echo "{\"Thresh\":\"0-50\",\"Result\":\"$FourCTRMod\"}" >> $CounterResults;
  echo "{\"Thresh\":\"0-60\",\"Result\":\"$FiveCTRMod\"}" >> $CounterResults;
  echo "{\"Thresh\":\"0-70\",\"Result\":\"$SixCTRMod\"}" >> $CounterResults;
  echo "{\"Thresh\":\"0-80\",\"Result\":\"$SevenCTRMod\"}" >> $CounterResults;
  echo "{\"Thresh\":\"0-90\",\"Result\":\"$EightCTRMod\"}" >> $CounterResults;
  echo "{\"Thresh\":\"0-100\",\"Result\":\"$NineCTRMod\"}" >> $CounterResults;
  echo "--------------------------------------";
  echo "{\"Thresh\":\"0-10\",\"Result\":\"$ZeroCTR\"}" >> $CounterResults;
  echo "{\"Thresh\":\"11-20\",\"Result\":\"$OneCTR\"}" >> $CounterResults;
  echo "{\"Thresh\":\"21-30\",\"Result\":\"$TwoCTR\"}" >> $CounterResults;
  echo "{\"Thresh\":\"31-40\",\"Result\":\"$ThreeCTR\"}" >> $CounterResults;
  echo "{\"Thresh\":\"41-50\",\"Result\":\"$FourCTR\"}" >> $CounterResults;
  echo "{\"Thresh\":\"51-60\",\"Result\":\"$FiveCTR\"}" >> $CounterResults;
  echo "{\"Thresh\":\"61-70\",\"Result\":\"$SixCTR\"}" >> $CounterResults;
  echo "{\"Thresh\":\"71-80\",\"Result\":\"$SevenCTR\"}" >> $CounterResults;
  echo "{\"Thresh\":\"81-90\",\"Result\":\"$EightCTR\"}" >> $CounterResults;
  echo "{\"Thresh\":\"91-100\",\"Result\":\"$NineCTR\"}" >> $CounterResults;

elif [ "$Mode" -eq "2" ]; then
  echo "Directory Comparison Mode";
  #TODO: Directory Comparison Mode!

else
  echo "Invalid Mode Selected: Mode == $Mode"
  exit 1;
fi

rm -rf zero-tmp.txt;
rm -rf one-tmp.txt;
rm -rf two-tmp.txt;
rm -rf three-tmp.txt;
rm -rf four-tmp.txt;
rm -rf five-tmp.txt;
rm -rf six-tmp.txt;
rm -rf seven-tmp.txt;
rm -rf eight-tmp.txt;
rm -rf nine-tmp.txt;

echo "------Moss Results Received------";
mv $ResultsDIR $ResultsDIR2 $ResultsTXT $CounterResults $MossResultsDIR;
#------------

echo "";
