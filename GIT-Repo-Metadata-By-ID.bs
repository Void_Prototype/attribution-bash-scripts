#!/bin/bash

IDFile="./female_dataset-logins.txt";
RepoMetadataOutput="Repo-Meta.txt";
GIT_OAUTH_TOKEN=$(<Oauth_Token.txt);
ResultsDIR="Repo-Data";

if [ -d $ResultsDIR ]; then
  echo "Directory '$ResultsDIR' exists. Re-Creating Directory";
  sleep 3s;
  echo "Recursively deleting directory '$ResultsDIR'";
  rm -rf $ResultsDIR;
fi

mkdir $ResultsDIR;

while IFS=, read Username;
do
  mkdir $ResultsDIR/$Username;
  curl -H "Authorization: token $GIT_OAUTH_TOKEN" https://api.github.com/users/$Username/repos >> $ResultsDIR/$Username/Repo-Data.txt;
  sleep 1s; 
done < $IDFile
