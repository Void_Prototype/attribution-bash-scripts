#!/bin/bash

Gender="Female";
Directory="./$Gender-Dataset-Unfiltered/*";

for d in $Directory; 
do
  [[ -d "$d" ]] && echo "${d##*/}" >> id-all-$Gender.txt;
done
