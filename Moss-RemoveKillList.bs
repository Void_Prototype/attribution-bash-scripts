#!/bin/bash

TmpDIR="TmpDir";
KillList="$TmpDIR/killlist.txt";


if [ -s $KillList ]; then

  while read target; do
    echo "Removing '$target'";
    rm $target;
  done < $KillList
  echo "DONE";
fi

