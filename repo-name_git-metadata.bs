#!/bin/bash

OUTPUTDIR="./REPONAME-METADATA"
GIT_OAUTH_TOKEN=$(<Oauth_Token.txt)
OUTPUTFILE="repo-meta.txt"
mkdir -p $OUTPUTDIR

while IFS=, read repo_name;
do
  mkdir -p $OUTPUTDIR/$repo_name;
  curl -H "Authorization: token $GIT_OAUTH_TOKEN" https://api.github.com/search/repositories?q=$repo_name+language:java >> $OUTPUTDIR/$repo_name/$OUTPUTFILE;
done < ./DB-repo_names.txt
